$(function(){
	var url=window.location.href;
	var subUrl=url.substring(url.length-3);
	//alert(subUrl);
	$('#daohang li').removeClass("on");
	$('#daohanguser li').removeClass("on");
	switch (subUrl) {
	case "nfo":$('#daohang1').addClass("on");break;
	case "art":$('#daohang2').addClass("on");break;
	case "ate":$('#daohang3').addClass("on");break;
	case "ent":$('#daohang4').addClass("on");break;
	case "log":$('#daohang5').addClass("on");break;
	default:$('#daohang1').addClass("on");break;
	}
	
	$('#updatePassword').on('click',function(){
		$('#updateUserPasswordModel').modal("show");
	});
	$('#updatePasswordSubmit').on('click',function(){
		var oldusername=$('#oldusername').val();
		var oldpassword=$('#oldpassword').val();
		if ($('#newpassword').val()!=$('#newpasswordS').val()) {
			alert('两次密码不一致');
			return;
		}
		var newpassword=$('#newpassword').val();
		var data={'oldusername':oldusername,'oldpassword':oldpassword,'newpassword':newpassword};
		$.ajax({
			url:"updateUserPassword",
			type:'post',
			dataType:'json',
			data:$.toJSON(data),
			contentType:'application/json;charset=utf-8',
			success:function(data){				
				if(data.status==1001){
					alert("用户密码修改成功");
				}
				else{
					alert("修改用户密码失败,请检查输入");
				}
			},
			error:function(){
				alert("请求失败");
			}
		});
	});
});