$(function(){
	jQuery('#updatePassword').on('click',function(){
		jQuery('#updateUserPasswordModel').modal("show");
		});
		jQuery('#updatePasswordSubmit').on('click',function(){
			var oldusername=$('#oldusername').val();
			var oldpassword=$('#oldpassword').val();
			if ($('#newpassword').val()!=$('#newpasswordS').val()) {
				alert('两次密码不一致');
				return;
			}
			var newpassword=$('#newpassword').val();
			var data={'oldusername':oldusername,'oldpassword':oldpassword,'newpassword':newpassword};
			$.ajax({
				url:"updateUserPassword",
				type:'post',
				dataType:'json',
				data:$.toJSON(data),
				contentType:'application/json;charset=utf-8',
				success:function(data){				
					if(data.status==1001){
						alert("用户密码修改成功");
					}
					else{
						alert("修改用户密码失败,请检查输入");
					}
				},
				error:function(){
					alert("请求失败");
				}
			});
		});
});