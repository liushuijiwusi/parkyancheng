(function($){
	$.fn.reserveview={};
	$.fn.reserveview.initial=function(){
		getData();
	};
	var getData=function(){
		$.ajax({
			url:"/parkYanCheng/reserve/getRecordsByName",
			type:'get',
			datatype: 'json',
			success:function(data){
				fillReserveBody(data);
			},
		});
	};
	var fillReserveBody=function(data){
		var reserveBody=$('#reserveBody');
		reserveBody.html('');
		data=data['body'];
		for(var i = 0; i < data.length; i++){
			var tr = $('<tr></tr>');
			tr.append('<td>' + data[i]['id']+ '</td>');
			tr.append('<td>' + data[i]['username']+ '</td>');
			tr.append('<td>' + data[i]['parkname']+ '</td>');
			tr.append('<td>' + data[i]['date']+ '</td>');
			reserveBody.append(tr);
		}
	}
})(jQuery);