package cn.park.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cn.park.model.Constants;
import cn.park.service.HttpUtil;

@Controller
public class MainController {

	@RequestMapping("/getServerRealPath")
	@ResponseBody
	public String index(HttpServletRequest request){
		//System.out.println(111);
		String path=request.getSession().getServletContext().getRealPath("");
		return path;
	}
	@RequestMapping("/logout")
	public String logout(HttpSession session){
		session.removeAttribute("username");
		session.removeAttribute("isAdmin");
		return "login";
	}
	@RequestMapping("/login")
	public String login(HttpSession session){
		//System.out.println(111);
		 if (session.getAttribute("username")==null||session.getAttribute("isAdmin")==null) {
				return "login";
			}
		 if ((boolean)session.getAttribute("isAdmin")==true) {
			return "parkinfo";
		}	
			return "parkinfomy";		
	}
	@RequestMapping("/map")
	public String map(ModelMap modelMap,HttpSession session,  HttpServletResponse response){
		if (session.getAttribute("username")==null||session.getAttribute("isAdmin")==null) {
			
		}
		else {
			modelMap.put("isLogin", "yes");
		}
	//	response.addHeader("Cache-Control", "no-cache, must-revalidate");
		return "map";
	}

	@RequestMapping("/parking")
	public String parking(){
		return "parking";
	}
	@RequestMapping("pay")
	public String pay(){
		return "pay";
	}
	@RequestMapping("/manage")
	public String manage(){
		return "parkinfo";
	}
	@RequestMapping("/register")
	public String register(){
		return "register";
	}
	@RequestMapping("/reserveview")
	public String reserveview(HttpSession session){
		 if ((boolean)session.getAttribute("isAdmin")==true) {
			return "reserveView";
			}	
			return "reserveViewMy";
	}
	@RequestMapping(value = "/registerYancheng", method = RequestMethod.POST, produces = "text/html; charset=UTF-8")
	@ResponseBody
	public Object registerYancheng(@RequestParam("username")String username,@RequestParam("password")String password){	
		String url = Constants.WEBAPIURL+"/registerYancheng";
		Map<String, Object> args=new HashMap<String, Object>();
		args.put("username", username);
		args.put("password", password);
		Map<String, Object> result = HttpUtil.post(url, args);
		Object data=result.get("body");
		 Gson gson = new Gson();
		 Map<String, Object> mapdata=gson.fromJson((String) data, new TypeToken<Map<String, Object>>(){
        }.getType() );
		 String status=(String) mapdata.get("status");
		 if (status.equals("1001")) {
			return "<script>alert('注册成功');window.location.href='login'</script>";
		}
		 else if (status.equals("1003")) {
			return "<script>alert('用户名已存在');</script>";
		}
		 else 
			 return "<script>alert('注册失败');</script>";
	}

	@RequestMapping(value = "/updateUserPassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Object updateUserPassword(@RequestBody Map<String, Object> args){
		String url =Constants.WEBAPIURL+"/updateUserPassword";
		Map<String, Object> result = HttpUtil.post(url, args);
		return result.get("body");
		
	}
}
